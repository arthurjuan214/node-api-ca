export interface IEncrypter{
  Encrypt(value: string): Promise<string>
}