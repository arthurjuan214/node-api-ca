import { IEmailValidator } from "../../presentation/Interfaces/IEmailValidator";
import validator from "validator";

export class EmailValidatorAdpater implements IEmailValidator  {
  
  IsValid(email: string): boolean {
     return validator.isEmail(email);
  }

}