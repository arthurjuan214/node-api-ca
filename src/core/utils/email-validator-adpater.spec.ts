import { EmailValidatorAdpater } from "./EmailValidatorAdapter";
import validator from 'validator';

jest.mock('validator', () =>({
  isEmail(): boolean{
    return true
  }
})
)

const makeSut = (): EmailValidatorAdpater =>{
  return new EmailValidatorAdpater();
}
describe("Email validator adapter", () => {
  test('Should return false if validator returns false', () => {
    const sut = makeSut();

    jest.spyOn(validator, 'isEmail').mockReturnValueOnce(false)
    const isValid =sut.IsValid('invalid_email@mail.com')
    expect(isValid).toBe(false);
  })


  test('Should return true if validator returns true', () => {
    const sut = makeSut();
    const isValid =sut.IsValid( 'valid_email@main.com')
    expect(isValid).toBe(true);
  })

  test('Should call validator with correct email', () => {
    const sut = makeSut();
    const isEmailSPy = jest.spyOn(validator, 'isEmail')
     sut.IsValid('any_email@main.com')
    expect(isEmailSPy).toHaveBeenCalledWith('any_email@main.com');
  })
})