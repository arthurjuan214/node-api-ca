import { AccountModel } from "../../../domain/entities/Account";
import { IAddAccountRepository } from "../../../Infra/AddAccountRepository/IAddAccountRepository";
import { IEncrypter } from "../../utils/encrypt/IEnrypter";
import { DbAddAccount } from "./DbAddAccount";
import { AddAccountDTO } from "./IAddAccount";

interface SutTypes {
  sut: DbAddAccount,
  encrypterStub: IEncrypter,
  AddAccountRepositoryStub: IAddAccountRepository
}

const makeEncrypter = (): IEncrypter => {
  class EncrypterStub implements IEncrypter{
    async Encrypt(value: string): Promise<string> {
      return new Promise(resolve => resolve('hashed_password'))
    }
  }

  return new EncrypterStub();
}

const makeAddAccountRepository = (): IAddAccountRepository => {
  class AddAccountRepositoryStub implements IAddAccountRepository{
    async Add(accountData: AddAccountDTO): Promise<AccountModel> {
      const fakeAccount = {
        id:'valid_id',
        name: 'valid_name',
        email:'valid_email',
        password: 'hashed_password'
      }
      return new Promise(resolve => resolve(fakeAccount))
    }
  }

  return new AddAccountRepositoryStub();
}

const makeSut = (): SutTypes =>{
 
  const encrypterStub = makeEncrypter();
  const AddAccountRepositoryStub = makeAddAccountRepository()
  const sut = new DbAddAccount(encrypterStub, AddAccountRepositoryStub);

  return {
    sut,
    encrypterStub,
    AddAccountRepositoryStub
  }
}

describe('DbAddAccount Usecase', () => { 
  test('Should call encrypter Encrypter with correct password', async ()=>{
  const  {sut, encrypterStub} = makeSut();
    const encryptSpy = jest.spyOn(encrypterStub, 'Encrypt')
    const accountDTO ={
      name: 'valid_name',
      email:'valid_email',
      password:'valid_pass',
    }

    await sut.Add(accountDTO)
    expect(encryptSpy).toHaveBeenCalledWith(accountDTO.password)
  })

  test('Should throw if Encrypter throws', async ()=>{
    const  {sut, encrypterStub} = makeSut();
      jest.spyOn(encrypterStub, 'Encrypt').mockReturnValueOnce(new Promise((resolve, reject) => reject(new Error())))
      const accountDTO ={
        name: 'valid_name',
        email:'valid_email',
        password:'valid_pass',
      }
  
      const promise =  sut.Add(accountDTO)
      await expect(promise).rejects.toThrow();
    })


    test('Should call AddAccountRepository with correct values', async ()=>{
      const  {sut, AddAccountRepositoryStub} = makeSut();
        const addSpy = jest.spyOn(AddAccountRepositoryStub, 'Add')
        const accountDTO ={
          name: 'valid_name',
          email:'valid_email',
          password:'valid_pass',
        }
    
        await sut.Add(accountDTO)
        await expect(addSpy).toHaveBeenCalledWith({
          name: 'valid_name',
          email:'valid_email',
          password:'hashed_password',
        })
      })




      test('Should throw if Encrypter throws', async ()=>{
        const  {sut, AddAccountRepositoryStub} = makeSut();
        jest.spyOn(AddAccountRepositoryStub, 'Add').mockReturnValueOnce(new Promise((resolve, reject) => reject(new Error())))
        const accountDTO ={
          name: 'valid_name',
          email:'valid_email',
          password:'valid_pass',
        }
    
        const promise =  sut.Add(accountDTO)
        await expect(promise).rejects.toThrow();
        })




        test('Should return a valid account ', async ()=>{
          const  {sut} = makeSut();
            const accountDTO ={
              name: 'valid_name',
              email:'valid_email',
              password:'valid_pass',
            }
        
            const account =   await sut.Add(accountDTO)
            expect(account).toEqual({
              id:'valid_id',
              name: 'valid_name',
              email:'valid_email',
              password:'hashed_password',
            })
          })

 })