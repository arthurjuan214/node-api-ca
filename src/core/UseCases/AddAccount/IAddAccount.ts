import { AccountModel } from "../../../domain/entities/Account"

export interface AddAccountDTO{
  name: string
  email: string
  password: string
}

export interface IAddAccount{
  Add(accountData: AddAccountDTO): Promise<AccountModel>
}