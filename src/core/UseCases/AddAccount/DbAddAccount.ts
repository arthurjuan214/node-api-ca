import { AccountModel } from "../../../domain/entities/Account";
import { IAddAccountRepository } from "../../../Infra/AddAccountRepository/IAddAccountRepository";
import { IEncrypter } from "../../utils/encrypt/IEnrypter";
import { AddAccountDTO, IAddAccount } from "./IAddAccount";

export class DbAddAccount implements IAddAccount{
  
  private readonly _encrypter: IEncrypter
  private readonly _addAccountRepository: IAddAccountRepository

  constructor(encrypter: IEncrypter, addAccountRepository: IAddAccountRepository){
    this._encrypter = encrypter;
    this._addAccountRepository = addAccountRepository;
  }
  
  
  async Add(accountData: AddAccountDTO): Promise<AccountModel> {
    
    const hashed_password = await this._encrypter.Encrypt(accountData.password);

    const account = await this._addAccountRepository.Add(Object.assign({}, accountData, {password: hashed_password}));  

    return account;
  }
}