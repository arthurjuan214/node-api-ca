import { ServerError } from "../erros/server-error"
import { IHttpResponse } from "../Interfaces/HttpContext"

export const BadRequest = (error) : IHttpResponse => {
  return {  statusCode: 400,
    body: error
    }
}

export const InternalServerError = (): IHttpResponse => {
  return {
    statusCode: 500,
    body: new ServerError
  }
}

export const Success = (data): IHttpResponse => {
  return {
    statusCode: 200,
    body: data
  }
}