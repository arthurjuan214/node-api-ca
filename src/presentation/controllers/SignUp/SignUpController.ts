import { IHttpRequest, IHttpResponse } from '../../Interfaces/HttpContext';
import { BadRequest, InternalServerError, Success } from '../../lib/HttpHelper';
import { MissingParamError, InvalidParamError } from "../../erros"
import { IAddAccount } from '../../../core/UseCases/AddAccount/IAddAccount';
import { IEmailValidator, IController } from './SignUpinterfaces';

export class SignUpController implements IController {

  private readonly _emailValidator: IEmailValidator 
  private readonly _addAccount: IAddAccount

  constructor(emailValidator: IEmailValidator, addAccount: IAddAccount) {
    this._emailValidator = emailValidator;
    this._addAccount = addAccount;
  }

async  Handle(httpRequest: IHttpRequest): Promise<IHttpResponse> {

    try {

      const requiredFields = ['name', 'email', 'password', 'passwordCheck'];

      for (const field of requiredFields) {

        if (!httpRequest.body[field]) {
          return BadRequest(new MissingParamError(field))
        }
      }
      
      const {name ,email, password, passwordCheck} = httpRequest.body;

      if(password != passwordCheck){
        return BadRequest(new InvalidParamError("Password confirmation"));
      }

      const isValid = this._emailValidator.IsValid(email);

      if (!isValid) {
        return BadRequest(new InvalidParamError("email"))
      }


      const account = await this._addAccount.Add({
        name,
        email,
        password,
      })

      return Success(account);

    } catch (error) {
      return InternalServerError();
    }

  }

}