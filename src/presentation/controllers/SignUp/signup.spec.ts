import { AddAccountDTO, IAddAccount } from '../../../core/UseCases/AddAccount/IAddAccount';
import { AccountModel } from '../../../domain/entities/Account';
import { InvalidParamError } from '../../erros/invalid-param-error';
import { MissingParamError } from '../../erros/missing-param-errors';
import { ServerError } from '../../erros/server-error';
import { IEmailValidator } from './SignUpinterfaces';
import {SignUpController} from './SignUpController'




const makeEmailValidator = (): IEmailValidator =>{
  class EmailValidatorStub implements IEmailValidator{
    IsValid(email: string): boolean{
      return true
    }
  }
return new EmailValidatorStub();
}

const makeAddAccount = (): IAddAccount =>{
  class AddAccountSub implements IAddAccount{
    async Add(account: AddAccountDTO): Promise<AccountModel>{
      const fakeAccount = {
        id: 'valid_id',
        name:'valid_name',
        email: 'valid_email@mail.com',
        password: 'valid_pass'
      }
      return new Promise(resolve => resolve(fakeAccount));
    }
  }
return new AddAccountSub();
}

interface SutTypes{
  sut: SignUpController,
  emailValidatorStub: IEmailValidator
  addAccountStub: IAddAccount
}

const makeSut = () : SutTypes => {

  const emailValidatorStub = makeEmailValidator();
  const addAccountStub = makeAddAccount()
  const sut =  new SignUpController(emailValidatorStub, addAccountStub)
  return {
    sut, emailValidatorStub, addAccountStub
  }
}

describe('SignUp Controller', ()=>{
  test('Should return 400 if no name is provided', async ()=>{
    const {sut} = makeSut();
    const httpRequest = {
      body:{
        email:'anymail@gmail.com',
        password:'pass',
        passwordCheck: 'pass'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError("name"));

  });

  test('Should return 400 if no email is provided', async ()=>{
    const {sut} = makeSut();
    const httpRequest = {
      body:{
        name:'any_name',
        password:'pass',
        passwordCheck: 'pass'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError("email"));

  })


  test('Should return 400 if no passowrd is provided', async ()=>{
    const {sut} = makeSut();
    const httpRequest = {
      body:{
        name: 'any_name',
        email:'anymail@gmail.com',
        passwordCheck: 'pass'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError("password"));

  });


  test('Should return 400 if no passowrd check is provided', async ()=>{
    const {sut} = makeSut();
    const httpRequest = {
      body:{
        name: 'any_name',
        email:'anymail@gmail.com',
        password: 'check'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError("passwordCheck"));

  });

  test('Should return 400 if no passowrd check fails', async ()=>{
    const {sut} = makeSut();
    const httpRequest = {
      body:{
        name: 'any_name',
        email:'anymail@gmail.com',
        password: 'pass',
        passwordCheck: 'wrong_pass'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError("Password confirmation"));

  });

  test('Should return 400 if an invalid email  is provided', async ()=>{
    const {sut, emailValidatorStub} = makeSut();
    jest.spyOn(emailValidatorStub, 'IsValid').mockReturnValueOnce(false);
    const httpRequest = {
      body:{
        name: 'any_name',
        email:'invalid_email',
        password: 'check',
        passwordCheck: 'check'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError("email"));

  });



  test('Should call EmailValitator with correct email', async ()=>{
    const {sut, emailValidatorStub} = makeSut();
    const isValidSpy = jest.spyOn(emailValidatorStub, 'IsValid')
    const httpRequest = {
      body:{
        name: 'any_name',
        email:'valid_email@email.com',
        password: 'check',
        passwordCheck: 'check'
      }
    } 
    await sut.Handle(httpRequest)
    expect(isValidSpy).toHaveBeenCalledWith("valid_email@email.com");

  });

  test('Should return 500 if email validator throws', async ()=>{

    class EmailValidatorStub implements IEmailValidator{
      IsValid(email: string): boolean{
          throw new Error();
          
      }
    }
  
    const emailValidatorStub = new EmailValidatorStub()
    const addAccountStub = makeAddAccount()
    const sut =  new SignUpController(emailValidatorStub, addAccountStub)


    const httpRequest = {
      body:{
        name: 'any_name',
        email:'any_email@email.com',
        password: 'check',
        passwordCheck: 'check'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)   
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError());

  });


 

  test('Should call AddAccount with correct values', async ()=>{

    const { sut, addAccountStub } = makeSut();
    const addSpy = jest.spyOn(addAccountStub, 'Add')

    const httpRequest = {
      body:{
        name: 'any_name',
        email:'any_email@email.com',
        password: 'check',
        passwordCheck: 'check'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(addSpy).toHaveBeenCalledWith({
        name: 'any_name',
        email:'any_email@email.com',
        password: 'check',
    })

  });


  test('Should return 500 if addAccount throws', async ()=>{

    const {sut, addAccountStub} = makeSut();
    jest.spyOn(addAccountStub, 'Add').mockImplementationOnce( async  ()=>{
      return new Promise((resolve, reject) => reject(new Error()))
      
    })


    const httpRequest = {
      body:{
        name: 'any_name',
        email:'any_email@email.com',
        password: 'check',
        passwordCheck: 'check'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError());

  });



  test('Should return 200 if addAccount success', async ()=>{

    const {sut, addAccountStub} = makeSut();
 

    const httpRequest = {
      body:{
        name: 'valid_name',
        email:'valid_email@mail.com',
        password: 'valid_pass',
        passwordCheck: 'valid_pass'
      }
    } 
    const httpResponse = await sut.Handle(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual({
      id: 'valid_id',
      name: 'valid_name',
      email:'valid_email@mail.com',
      password: 'valid_pass',
    });

  });

})