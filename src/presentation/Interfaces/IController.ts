import { IHttpRequest, IHttpResponse } from "./HttpContext";

export interface IController{
    Handle(httpRequest: IHttpRequest): Promise<IHttpResponse>
}