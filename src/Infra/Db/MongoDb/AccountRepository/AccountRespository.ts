import { AddAccountDTO } from "../../../../core/UseCases/AddAccount/IAddAccount";
import { AccountModel } from "../../../../domain/entities/Account";
import { IAddAccountRepository } from "../../../AddAccountRepository/IAddAccountRepository";
import { MongoHelper } from "../Helpers/Helpers";

export class AccountMongoRepository implements IAddAccountRepository {
  async Add(accountData: AddAccountDTO): Promise<AccountModel>{
    
    const accountCollection = MongoHelper.GetCollection('accounts');
    const result = await accountCollection.insertOne(accountData);

    const accountId = result.insertedId;
    const accountDb = await accountCollection.findOne({_id: accountId});

    const account = MongoHelper._mapper(accountDb);

    return account;
  }
}