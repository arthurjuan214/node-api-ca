import { AccountMongoRepository } from "./AccountRespository";
import { MongoHelper } from "../Helpers/Helpers";

const makeSut = (): AccountMongoRepository =>{
  return new AccountMongoRepository();
}

describe("Account MongoDB Repository", () => {

  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL);
   });
   
   afterAll(async () => {
     await MongoHelper.disconnect();
   });  

   beforeEach( async () =>{
  const accountCollection = MongoHelper.GetCollection('accounts');
  await accountCollection.deleteMany({});
   } )

  test('Should return an account on success', async ()=>{
    const sut = makeSut();
    const accountDTO = {
      name: 'any_name',
      email: 'any_email@mail.com',
      password: 'any_password'
    } 
    const account = await sut.Add(accountDTO)

    expect(account).toBeTruthy();
    expect(account.id).toBeTruthy();
    expect(account.name).toBe(accountDTO.name)
    expect(account.email).toBe(accountDTO.email)
    expect(account.password).toBe(accountDTO.password)


  } )
})