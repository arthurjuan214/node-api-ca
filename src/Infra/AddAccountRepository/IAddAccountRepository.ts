import { AccountModel } from "../../domain/entities/Account";
import { AddAccountDTO } from "../../core/UseCases/AddAccount/IAddAccount";

export interface IAddAccountRepository{
  Add(accountData: AddAccountDTO): Promise<AccountModel>
}