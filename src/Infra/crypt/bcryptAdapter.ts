import { IEncrypter } from "../../core/utils/encrypt/IEnrypter";
import bcrypt from 'bcrypt';
export class BcryptAdapter implements IEncrypter {

  private readonly salt: number;
  constructor(salt: number){
    this.salt = salt;
  }

  async Encrypt(value: string): Promise<string> {
    const hashString = await bcrypt.hash(value, this.salt);
    return hashString;
  }
}