import bcrypt from 'bcrypt';
import { BcryptAdapter } from './bcryptAdapter';

const salt = 12;
const makeSut = (salt):BcryptAdapter =>{
  return new BcryptAdapter(salt)
}

jest.mock('bcrypt', ()=>({
  async hash(): Promise<string>{
    return new Promise(resolve => resolve('hash'))
  }
}));

describe('bcrypt Adapter tests', ()=>{
  test('Should calls bcrypt with corret value', async ()=>{
    const sut = makeSut(salt);
    const hashSpy = jest.spyOn(bcrypt, 'hash')
    await sut.Encrypt('any_value');

    expect(hashSpy).toHaveBeenCalledWith('any_value', salt);

  })

  test('Should return a hash on success', async ()=>{
    const sut = makeSut(salt);
    const hash = await sut.Encrypt('any_value');

    expect(hash).toBe('hash')

  })

  test('Should throws if bcrypt throws', async ()=>{

    const sut = makeSut(salt);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //@ts-ignore
    jest.spyOn(bcrypt, 'hash').mockRejectedValueOnce( new Error() )
    const promise =  sut.Encrypt('any_value');
    await expect(promise).rejects.toThrowError();
  })

}); 