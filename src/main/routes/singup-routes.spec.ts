import { MongoHelper } from "../../Infra/Db/MongoDb/Helpers/Helpers";
import request from 'supertest';
import app from "../config/app";

describe('signup routes', ()=>{
  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL);
   });
   
   afterAll(async () => {
     await MongoHelper.disconnect();
   });  

   beforeEach( async () =>{
  const accountCollection = MongoHelper.GetCollection('accounts');
  await accountCollection.deleteMany({});
   } );

   test('should return an account on success', async () =>{
    await request(app)
    .post('/api/signup')
    .send({
      name: 'Rodrigo',
      email: 'major.rd@gmail.com',
      password: 'rockdanger',
      passwordCheck: 'rockdanger'
    }).expect(200);
   });
});