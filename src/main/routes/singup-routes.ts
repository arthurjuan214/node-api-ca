import { Router, Response, Request } from "express";
import { AdaptRoute } from "../Adapter/express-routes-adapter";
import { makeSignUpController } from "../factories/signup";
export default (router: Router): void =>{
  router.post("/signup", AdaptRoute(makeSignUpController()) )
  router.get("/health", (req: Request, res: Response) =>{
    return res.json({
      status: 'ok'
    })
  })
}