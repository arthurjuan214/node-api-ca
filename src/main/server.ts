import app from './config/app'
import { MongoHelper } from '../Infra/Db/MongoDb/Helpers/Helpers';
import env from './config/env'

console.log(env.mongoUrl);
MongoHelper.connect("mongodb://localhost:27017/clean-node-api")
.then( ()=>{
  app.listen(env.port, ()=> console.log(`server runnig on http://localhost:${env.port}`))
})
.catch(console.error);
