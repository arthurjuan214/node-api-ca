import { Request, Response } from "express";
import app from "../config/app";
import request from 'supertest'

describe("cors test", ()=>{

  test("should enable cors", async ()=>{
    app.get('/test-cors', (req: Request, res: Response) => {
      res.send();
    })

    await request(app).get('/test-cors').expect('access-control-allow-origin', '*');
    await request(app).get('/test-cors').expect('access-control-allow-methods', '*');
    await request(app).get('/test-cors').expect('access-control-allow-headers', '*');

  })


});