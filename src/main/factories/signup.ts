import { SignUpController } from "../../presentation/controllers/SignUp/SignUpController"
import { EmailValidatorAdpater } from "../../core/utils/EmailValidatorAdapter";
import { DbAddAccount } from "../../core/UseCases/AddAccount/DbAddAccount";
import { BcryptAdapter } from "../../Infra/crypt/bcryptAdapter";
import { AccountMongoRepository } from "../../Infra/Db/MongoDb/AccountRepository/AccountRespository";

export const makeSignUpController = (): SignUpController => {
  const emailValidatorAdapter = new EmailValidatorAdpater();
  const bcryptAdapter = new BcryptAdapter(12);
  const accountRepo = new AccountMongoRepository();
  const dbAddAccount = new DbAddAccount(bcryptAdapter, accountRepo )

  const signUpController = new SignUpController(emailValidatorAdapter, dbAddAccount);
  return signUpController;
}  

