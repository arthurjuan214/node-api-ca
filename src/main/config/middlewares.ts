import express, { Express } from 'express'
import { cors } from '../middlewares'

export default (app: Express): void =>{
  app.use(express.json())
  app.use(cors);
}