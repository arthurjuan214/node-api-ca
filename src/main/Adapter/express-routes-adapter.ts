import { IController, IHttpRequest } from "../../presentation/Interfaces";
import { Request, Response } from "express";

export const AdaptRoute = (controller: IController) =>{
  return async (req: Request, res: Response) => {
    const httpRequest : IHttpRequest = { 
      body: req.body
    }
    const httpResponse = await controller.Handle(httpRequest)
    res.status(httpResponse.statusCode).json(httpResponse.body);
  }
} 